package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"syscall"
	"time"
)

func handleRoot(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	fragment := r.URL.Fragment
	if len(query) == 0 && len(fragment) == 0 {
		fmt.Fprintf(w, "Hello world!")
		return
	}
	str := "Hello from request handler:"
	if len(query) != 0 {
		for k, v := range query {
			str += " query elem: [" + k + ": " + strings.Join(append([]string{}, v...), ",") + "] "
		}
	}
	if len(fragment) != 0 {
		str += " fragment: " + fragment
	}
	fmt.Fprintf(w, str)
}

func getPort() string {
	portFromEnv := os.Getenv("PORT")
	if len(portFromEnv) == 0 {
		return "8080"
	}
	return portFromEnv
}

func main() {
	log.Println("[debug] configuring server...")
	mux := http.NewServeMux()
	mux.HandleFunc("/", handleRoot)

	server := &http.Server{
		Addr:    ":" + getPort(),
		Handler: mux,
	}
	server.SetKeepAlivesEnabled(false)
	go func() {
		log.Println("[debug] starting server...")
		err := server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			fmt.Fprintf(os.Stderr, err.Error())
		}
		log.Println("[debug] server stopped")
	}()

	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)

	<-signalChan

	ctx, done := context.WithTimeout(context.Background(), 5 * time.Second)
	defer done()

	log.Println("[debug] stopping server...")
	err := server.Shutdown(ctx)
	if err != nil {
		fmt.Fprintf(os.Stderr, err.Error())
	}
}
