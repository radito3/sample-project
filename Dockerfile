FROM golang:1.17-alpine3.14 as build

WORKDIR /app
COPY main.go go.mod ./
RUN go build -o server

FROM alpine:3.14

RUN apk add --no-cache ca-certificates

COPY --from=build /app/server /server

EXPOSE 8080
ENTRYPOINT [ "/server" ]
